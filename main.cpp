/**
    Author: O-Marius Chincisan

    $1.00US License for corporations|business/device. Free for personal use.
    This program should run as root.

    Every time a characterisitc/service is chnaged,
    turn off and on on mobile the BT to clear the cached LE's.

	ONLY FOR BROADCOM DONGLE BCM20702A
    http://plugable.com/2014/06/23/plugable-usb-bluetooth-adapter-solving-hfphsp-profile-issues-on-linux
    Newer Kernel Versions (3.16 and later)
    wget https://s3.amazonaws.com/plugable/bin/fw-0a5c_21e8.hcd
    sudo mkdir /lib/firmware/brcm
    sudo mv fw-0a5c_21e8.hcd /lib/firmware/brcm/BCM20702A0-0a5c-21e8.hcd

    This demo adds 1 service 0x123F with 3 characteristis.
        0x3400  control a GPIO pin, we connect a LED, on GPIO 17 '/sys/class/gpio/gpio17/value'
        0x3401
        0x3402

    Might require libcrypto++ to be installed
    Get lib bunget from : https://github.com/comarius/bunget
*/

#include <fstream>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <uuid.h>
#include <libbunget.h>

using namespace std;
using namespace bunget;

bool __alive = true;  //required by lib set it to false to break run()

/**
    Command Line parameters:
    -nnotification-file-binary
    -tmsecs-for-notficiations
    -aname:0  adv name:hci#
      -sUUID      
          -cUUID
              -pWRIN
              -sWRIN
              -vc++cype[elems]:defaultvalues,...
            ...
   ...


    the parameters passed to notification binary files are:
    -sUUID -sUUID...   when remote discoveres the server services.
    -rUUID               when remote reads request. Binary should stdout the value for UUID
    -iUUID               when remote needs indicator. Binary should stdout the value for UUID
    -nUUID               when time to send notification. Binary should stdout the value for UUID
    -wUUID:VALUE         remove write reaquest
    -dUUID:VALUE         descriptor chnaged. test if remote wrote notification descriptor.
    -aON/OFF             adapter on /off
    -cMAC                MAC present, a client connected, otherwise gone.
*/

#define UID_GPIO    0x3400
#define UID_TIME    0x3401
#define UID_TEMP    0x3403

class my_proc : public ISrvProc
{
public:
    my_proc();
    void onServicesDiscovered(std::vector<IHandler*>& els);
    void onReadRequest(IHandler* pc);
    int evSubscribesNotify(IHandler* pc, bool b);
    void onIndicate(IHandler* pc);
    void onWriteRequest(IHandler* pc);
    void onWriteDescriptor(IHandler* pc, IHandler* pd);
    void evAdvertise(bool onoff);
    void evDeviceState(bool onoff);
    void evClientState(bool connected);
    bool evLoop(IServer* ps);

private:
    void        _prepare_gpio17();
    const char* _get_time();
    float       _get_temp();
    const char* _get_temp_s();
    uint8_t     _get_gpio();
    void        _send_value(IHandler* pc);

public:
    char        _some[20];
    bool        _subscribed;
    IHandler*   LedChr;       // RW
    IHandler*   TimeChr;      // NIR
    IHandler*   Temp1Chr;     // NIR
};



int main(int n, char* v[])
{
    BtCtx*      ctx = BtCtx::instance();
    my_proc     procedure;

    IServer*    BS =  ctx->new_server(&procedure, 0, "rpi-bunget", 0);
    IService*   ps = BS->add_service(0x123F,"demo");
    procedure.LedChr = ps->add_charact(UID_GPIO,PROPERTY_WRITE|PROPERTY_INDICATE|PROPERTY_NOTIFY,
                             0,
                             FORMAT_RAW, 1); // 1 / 0

    procedure.TimeChr = ps->add_charact(UID_TIME, PROPERTY_READ|PROPERTY_NOTIFY,
                             0,
                             FORMAT_RAW, 20); // we send it as string

    procedure.Temp1Chr = ps->add_charact(UID_TEMP, PROPERTY_NOTIFY|PROPERTY_INDICATE,
                              0,
                              FORMAT_RAW, 20); // we send it as string too for demo

    try{
        BS->advertise(true);
        BS->run();
    }
    catch(hexecption& ex)
    {
        TRACE(ex.report());
    }
    return 0;
}

my_proc::my_proc()
{
    _subscribed=false;
    _prepare_gpio17();
}

bool my_proc::evLoop(IServer* ps)
{
    static int inawhile=0;

    // notification
    if(_subscribed && inawhile++%600==0)
    {
        _send_value(TimeChr);
        _send_value(Temp1Chr);
    }
    return true;
}

void my_proc::onServicesDiscovered(std::vector<IHandler*>& els)
{
    TRACE("my_proc event: onServicesDiscovered");
}

/// remote readsd pc characteritcis
void my_proc::onReadRequest(IHandler* pc)
{
    TRACE("my_proc event:  onReadRequest:" <<  std::hex<< pc->get_16uid() << std::dec);
    _send_value(pc);
}

int my_proc::evSubscribesNotify(IHandler* pc, bool b)
{
    TRACE("my_proc event: evSubscribesNotify:" << std::hex<< pc->get_16uid() << "="<<(int)b<< std::dec);
    _subscribed = b;
    return 0 ;
}

void my_proc::onIndicate(IHandler* pc)
{

    TRACE("my_proc event:  onIndicate:" <<  std::hex<< pc->get_16uid() << std::dec);
    _send_value(pc);
}

void my_proc::onWriteRequest(IHandler* pc)
{
    TRACE("my_proc event:  onWriteRequest:" <<  std::hex<< pc->get_16uid() << std::dec);
    std::string     ret;
    const uint8_t*  value = pc->get_value();
    char            by[4];
    int             i=0;

    for(;i<pc->get_length();i++)
    {
        ::sprintf(by,"%02X:",value[i]);
        ret.append(by);
    }
    TRACE("Remote data:" << ret);
    if(pc->get_16uid() == UID_GPIO)
    {
        if(::access("/sys/class/gpio/gpio17/value",0)==0)
        {
            if(value[0]==0)
                system("echo 0 > /sys/class/gpio/gpio17/value");
            else
                system("echo 1 > /sys/class/gpio/gpio17/value");
        }
    }
}

//descriptor chnaged of the charact
void my_proc::onWriteDescriptor(IHandler* pc, IHandler* pd)
{
    TRACE("my_proc event:  onWriteDescriptor:" << int(*((int*)(pd->get_value()))));
}


void my_proc::evAdvertise(bool onoff)
{
    TRACE("my_proc event:  evAdvertise:" << onoff);
}

void my_proc::evDeviceState(bool onoff)
{
    TRACE("my_proc event:  evDeviceState:" << onoff);
    if(onoff==false)
    {
        _subscribed = false;
    }
}

void my_proc::evClientState(bool connected)
{
    if(connected==false)
    {
        _subscribed = false;
        TRACE("my_proc event: onClientDisconnected");
       // _alive=false;
    }
}

void my_proc::_prepare_gpio17()
{
    system ("chmod 777 /sys/class/gpio/export");
    system ("echo 17 > /sys/class/gpio/export");
    system ("chmod 777 /sys/class/gpio/gpio17/*");
    system ("sync");
}

const char*  my_proc::_get_time()
{
    time_t secs = time(0);
    struct tm *local = localtime(&secs);
    sprintf(_some, "%02d:%02d:%02d", local->tm_hour, local->tm_min, local->tm_sec);
    return _some;
}

float my_proc::_get_temp()
{
    float ftamp=0.0;
#ifdef ARM_CC
    ::system("/opt/vc/bin/vcgencmd measure_temp > /tmp/bunget");
    std::ifstream ifs("/tmp/bunget");
    std::string temp( (std::istreambuf_iterator<char>(ifs) ),(std::istreambuf_iterator<char>()));
    temp = temp.substr(5);
    ftamp =::atof(temp.c_str());

#else //fake it
    std::string temp = "temp=32.5";
    temp = temp.substr(5);
    ftamp =::atof(temp.c_str());
    ftamp += rand()%5;
#endif
    return ftamp;
}

const char* my_proc::_get_temp_s()
{
   float ftamp=0.0;
#ifdef ARM_CC
    ::system("/opt/vc/bin/vcgencmd measure_temp > /tmp/bunget");
    std::ifstream ifs("/tmp/bunget");
    std::string temp( (std::istreambuf_iterator<char>(ifs) ),(std::istreambuf_iterator<char>()));
    ::strcpy(_some,temp.c_str());
#else //fake it
    ::sprintf(_some,"temp=%d.%d", rand()%40,rand()%100);
#endif
    return _some;
}

uint8_t my_proc::_get_gpio()
{
    if(::access("/sys/class/gpio/gpio17/value",0)==0)
    {
        std::ifstream ifs("/sys/class/gpio/gpio17/value");
        std::string temp( (std::istreambuf_iterator<char>(ifs) ),(std::istreambuf_iterator<char>()));
        return uint8_t(::atoi(temp.c_str()));
    }

    return 0;
}

void my_proc::_send_value(IHandler* pc)
{
    uint16_t uid = pc->get_16uid();
    switch(uid)
    {
        case  UID_GPIO:
            {
                uint8_t gp = _get_gpio();
                pc->put_value((uint8_t*)&gp,1);
            }
            break;
        case  UID_TIME:
            {
                const char* t = _get_time();
                pc->put_value((uint8_t*)t,::strlen(t));
            }
            break;
        case  UID_TEMP:
            {
                float ft = _get_temp();
                //pc->put_value((uint8_t*)&fts,sizeof(float));
                const char* fts = _get_temp_s();
                pc->put_value((uint8_t*)fts,::strlen(fts));
            }
            break;
        default:
            break;
    }
}
