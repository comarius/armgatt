#ifndef _LIB_bunget_H_
#define _LIB_bunget_H_

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <cerrno>
#include "uuid.h"
#include "bluetooth.h"

/// these are the mumbo jumbo protocol shitty properties
#define  PROPERTY_BROADCAST                 0x1 //
#define  PROPERTY_READ                      0x2 //
#define  PROPERTY_WRITE_NO_RESPONSE         0x4 //
#define  PROPERTY_WRITE                     0x8 //  client chnages values in the server
#define  PROPERTY_NOTIFY                    0x10 // ATT_NOTIFYCATION + value. client no ack back as  PROPERTY_WRITE_NO_RESPONSE
#define  PROPERTY_INDICATE                  0x20 // requires a ack from client, as confirmation. server cannot send another indicator
                                                 // untill receives confirmation
#define  PROPERTY_SIGNED_WRITE              0x40 //
#define  PROPERTY_EXTENDED_PROPS            0x80 //


#define  SECURITY_BROADCAST                 0x1
#define  SECURITY_READ                      0x2
#define  SECURITY_WRITE_NO_RESPONSE         0x4
#define  SECURITY_WRITE                     0x8
#define  SECURITY_NOTIFY                    0x10
#define  SECURITY_INDICATE                  0x20
#define  SECURITY_SIGNED_WRITE              0x40
#define  SECURITY_EXTENDED_PROPS            0x80

#define  PERMISSION_NA                      0
#define  PERMISSION_READ                    1
#define  PERMISSION_READ_ENCRYPTED          2
#define  PERMISSION_READ_ENCRYPTED_MITM     4
#define  PERMISSION_WRITE                   16
#define  PERMISSION_WRITE_ENCRYPTED         32
#define  PERMISSION_WRITE_ENCRYPTED_MITM    64
#define  PERMISSION_WRITE_SIGNED            128
#define  PERMISSION_WRITE_SIGNED_MITM       256

#define  WRITE_TYPE_DEFAULT                 2
#define  WRITE_TYPE_NO_RESPONSE             1
#define  WRITE_TYPE_SIGNED  4

#define  FORMAT_UINT8       17
#define  FORMAT_UINT16      18
#define  FORMAT_UINT32      20
#define  FORMAT_SINT8       33
#define  FORMAT_SINT16      34
#define  FORMAT_SINT32      36
#define  FORMAT_SFLOAT      50
#define  FORMAT_FLOAT       52
#define  FORMAT_RAW         0

namespace bunget
{
typedef enum H_ATT_TYPE {H_NOTYPE=0,
                         H_SRV=0x1,
                         H_SRV_INC=0x2,
                         H_CHR=0x4,
                         H_CHR_VALUE=0x8,
                         H_DSC=0x10,
                         H_ATTRIB=0x20
                        } H_ATT_TYPE;

enum E_BT_MODE {eCLIENT, eSERVER};


class hexecption
{
public:

    hexecption(const char* f, const char* fu, int err, const char* ex=0):_f(f),_fu(fu),_ex(err)
    {
        if(ex)
        {
            _usr=ex;
        }

    };
    std::string report()
    {
        char se[8];
        ::sprintf(se,"%d",_ex);
        std::string rep = _usr+ " " + _f + ", "+ _fu + ", "+se;
        return rep;
    }
    std::string _f;
    std::string _fu;
    std::string _usr;
    int         _ex;
};

class IService;
class IHandler
{
public:
    virtual ~IHandler() {}

    virtual IHandler* add_descriptor(uint16_t uid, uint8_t prop, uint8_t* value, int len)=0;
    virtual IHandler* add_descriptor(const bt_uuid_t& uid, uint8_t prop, uint8_t* value, int len)=0;
    virtual IService* get_service()const=0;
    virtual uint16_t get_16uid()const=0;
    virtual const bt_uuid_t& get_128uid()const=0;
    virtual uint8_t get_props()const=0;
    virtual uint8_t get_perms()const=0;
    virtual uint8_t get_format()const=0;
    virtual uint8_t get_length()const=0;
    virtual const uint8_t* get_value()const=0;
    virtual int put_value(const uint8_t* v, size_t len)=0;
    virtual H_ATT_TYPE get_type()const=0;
};

class IService
{
public:
    virtual ~IService() {}
    virtual IHandler*    add_charact(uint16_t uid, uint8_t prop, uint8_t perms, uint8_t format, uint8_t length, uint8_t *val=0)=0;
    virtual IHandler*    add_charact(const bt_uuid_t& srvid, uint8_t prop, uint8_t perms, uint8_t format, uint8_t length, uint8_t *val=0)=0;
    virtual IHandler*    get_charact(const bt_uuid_t& srvid)=0;
    virtual IHandler*    get_charact(uint32_t uid)=0;
    virtual const bt_uuid_t&   get_uid()const=0;
};

class Cdev
{
public:
    std::string _mac;
    std::string _name;
    uint8_t     _props;
};

class ISrvProc;
class IServer
{
public:
    virtual ~IServer() {}
    virtual void    power_off()=0;
    virtual int    advertise(bool onoff)=0; ///returns device hci#
    virtual IService* add_service(uint16_t srvid, const char* name)=0;
    virtual IService* add_service(const bt_uuid_t& srvid, const char* name)=0;
    virtual IService* get_service(const bt_uuid_t& srvid)=0;
    virtual IService* get_service(uint16_t srvid)=0;
    virtual void run()=0;//uv_run(uv_default_loop(), UV_RUN_DEFAULT);


    virtual void getVersion(uint8_t& hciver, uint16_t& hcirev, uint8_t& lmpver, uint16_t& man, uint16_t& lmpsubver)=0;
    virtual const bdaddr_t& getAddr()const = 0;
};

class ISrvProc
{
public:
    virtual ~ISrvProc() {}

    virtual void onServicesDiscovered(std::vector<IHandler*>& els)=0;
    virtual bool evLoop(IServer* ps)=0;
    virtual void onReadRequest(IHandler* pc)=0;
    virtual int  evSubscribesNotify(IHandler* pc, bool b)=0;
    virtual void onWriteRequest(IHandler* pc)=0;
    virtual void onWriteDescriptor(IHandler* pc, IHandler* pd)=0;
    virtual void onIndicate(IHandler* pc)=0;

    virtual void evAdvertise(bool onoff)=0;
    virtual void evDeviceState(bool onoff)=0;
    virtual void evClientState(bool onoff)=0;
};

class BtCtx
{
public:
    static BtCtx* instance();
    virtual IServer* new_server(ISrvProc* proc, int hcidev, const char* name, int pin)=0;
};


};

#endif //_LIB_bunget_H_
