## BTLE FOR Raspberry PI USING BUNGET C++ GATT LIBRARY ##

Demo shows one GATT service with 3 characteristics. 


* One characteristic controls GPIO 17

* One charateristics reads the system time

* Last charateristic reads the onboard temperature sensor.

* Use a reliable BTLE app. iOS punchtru' BTLE scanner mismatches the notifications IIDS!!!


#### GATT characteristics read by arduuipush apk on Android tablet
![gatt.png](https://bitbucket.org/repo/4kLXA4/images/3493780247-gatt.png)


Hope it helps.

### 
For bunget lib see https://github.com/comarius ###